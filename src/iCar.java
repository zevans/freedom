
public interface iCar {
	
	String getMake();
	
	String getModel();
	
	int	getYear();
	
	int getMileage();
	
}
